package lab;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in);
		Random rand = new Random();
		int number =rand.nextInt(100);
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt();
		int attemptCounter = 1;

		while (guess != -1) {
			if (number == guess) {
				System.out.println("Congratulations! You won after "+ attemptCounter+" attempts!");

				break;
			}
			else{
				System.out.println("Sorry!");
				if (number > guess)
					System.out.println("Mine is a greater than your guess ...");
				else
					System.out.println("Mine is a less than your guess ...");
				System.out.print("Type -1 to quit or guess another: ");
				guess = reader.nextInt();
				attemptCounter++;
			}
		}

		if (number != guess)
			System.out.println("Sorry, the number was " + number);

		reader.close();
	}
}
