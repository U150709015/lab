package lab;


public class FindPrime {
    public static void main(String[] args) {
        //Print the prime numbers less then the given number

        //Get the number
        int given = Integer.parseInt(args[0]);
        //For each number less than the given
        for (int number = 2; number < given; number++) {
            boolean isPrime = true;
            //for each divisor less than number
            for(int divisor = 2;  divisor < number; divisor++)
                //if number is divisible by divisor
                if (number % divisor == 0) {
                    //is prime is false
                    isPrime = false;
                    //end the loop
                    break;
                }


            //Check the number is prime or not

            //if the number is prime
            if (isPrime)
            //print the number
                System.out.println(number + ", ");

        }
    }
}
