public class FindMaximum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int result;

        boolean someCondition = value1 > value2;

        result = someCondition ? value1 : value2; // eğer condition true ise value1 değeri output'dur. Eğer false ise value2 output

        System.out.println(result);

	}

	}
